###########################################################################
# Copyright (C) 2015-2019 "IoT.bzh"
#
# author: José Bollo <jose.bollo@iot.bzh>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###########################################################################

pkg_check_modules(json REQUIRED json-c)
pkg_check_modules(ssl REQUIRED openssl)
pkg_check_modules(mhd REQUIRED libmicrohttpd>=0.9.60)
pkg_check_modules(curl REQUIRED libcurl)
pkg_check_modules(cyna REQUIRED cynagora)

# for the server
if(DEFINED DEFAULTPORT)
  add_definitions(-DDEFAULTPORT="${DEFAULTPORT}")
endif()
if(DEFINED DEFAULTHOSTS)
  add_definitions(-DDEFAULTHOSTS="${DEFAULTHOSTS}")
endif()
if(DEFINED ENVHOSTS)
  add_definitions(-DENVHOSTS="${ENVHOSTS}")
endif()

# for the launcher
if(DEFINED DEFAULTNAME)
  add_definitions(-DDEFAULTNAME="${DEFAULTNAME}")
endif()
if(DEFINED VARURL)
  add_definitions(-DVARURL="${VARURL}")
endif()
if(DEFINED DEFAULTURL)
  add_definitions(-DDEFAULTURL="${DEFAULTURL}")
endif()

add_executable(
  cynagoauth-server
    cynagoauth-server.c
    uid.c
    base64.c
    escape.c
    cred.c
    crypt.c
)
target_link_libraries(
  cynagoauth-server
    ${json_LDFLAGS}
    ${ssl_LDFLAGS}
    ${mhd_LDFLAGS}
    ${cyna_LDFLAGS}
)
install(
  TARGETS
    cynagoauth-server 
  DESTINATION
    ${CMAKE_INSTALL_BINDIR}
)

add_executable(
  cynagoauth-launch
    cynagoauth-launch.c
    curl-wrap.c
    escape.c
)
target_link_libraries(
  cynagoauth-launch
    ${curl_LDFLAGS}
    ${json_LDFLAGS}
)

install(
  TARGETS
    cynagoauth-launch
  DESTINATION
    ${CMAKE_INSTALL_BINDIR}
)
