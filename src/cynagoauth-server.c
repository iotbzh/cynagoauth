/*
 * Copyright (C) 2018 "IoT.bzh"
 * Author: José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <poll.h>
#include <netdb.h>
#include <sys/socket.h>
#include <getopt.h>

#include <microhttpd.h>
#include <json-c/json.h>
#include <cynagora.h>

#include "cred.h"
#include "uid.h"
#include "escape.h"
#include "crypt.h"
#include "base64.h"

#define DUMP		0
#define UNKNOWN		1
#define AUTH_CODE	0
#define AUTH_IMPL	0
#define TOK_CODE	0
#define TOK_CLIENT	1

#if !defined(DEFAULTPORT)
#    define DEFAULTPORT		"7777"
#endif
#if !defined(DEFAULTHOSTS)
#    define DEFAULTHOSTS	"*:"DEFAULTPORT
#endif
#if !defined(ENVHOSTS)
#    define ENVHOSTS		"CYNAGOAUTH_HOSTS_SPEC"
#endif

static const char shortopts[] = "hs";

static const struct option longopts[] = {
	{ "help",     0, 0, 'h' },
	{ "secure",   0, 0, 's' },
	{ "unsecure", 0, 0, 'u' },
	{ 0, 0, 0, 0 }
};

const char helpmsg[] =
	"\n"
	"usage: %s [options...] [interfaces...]\n"
	"\n"
	"Run a basic OAuth server, currently only implementing client credential\n"
	"flow based on Smack labels and Cynagora backend.\n"
	"\n"
	"The interfaces specify ip adresses and port to listen. It must be of\n"
	"the form [HOST][:SERVICE]. Default host: *, default port: "DEFAULTPORT".\n"
	"Examples:\n"
	"\n"
	"  localhost:5555        listen on loopback on port 5555\n"
	"  *:1234                listen any interface on port 1234\n"
	"  localhost             listen on default port of localhost\n"
	"\n"
	"Default interface if none is given: "DEFAULTHOSTS"\n"
	"\n"
	"Options:\n"
	"\n"
	" -h, --help        this help\n"
	" -s, --secure      serves https\n"
	" -u, --unsecure    serves http\n"
	"\n"
;

void printhelp(char *prog)
{
	prog = strchr(prog, '/') ? strrchr(prog, '/') + 1 : prog;
	printf(helpmsg, prog);
	exit(0);
}

static char cert[] =
"-----BEGIN CERTIFICATE-----\n"
"MIID5TCCAk2gAwIBAgIUBJtxecHlDMYCJfnDLqo8iWmNREkwDQYJKoZIhvcNAQEL\n"
"BQAwADAgFw0xODEyMDQxNDA1MDZaGA8yMjY1MDUwMzE0MDUxN1owADCCAaIwDQYJ\n"
"KoZIhvcNAQEBBQADggGPADCCAYoCggGBAPC8qdhly0wFUL+EG5t0WcjHPr7y1BKo\n"
"0KPQZdBskU2EkmoN3L92v37VDY6SFralEUiqFPU1DSUFREe8gEZiNUbjQMati4CZ\n"
"yUZM1i4HYjTd15WLF4HPepoVcaNiZZSK3Agjt0iBOvuF17ZXB4GJXIzGojR3z+j3\n"
"FGIRHj0vWW782hEXPcCtufsMOVpcOJsMmqMbYS640t0PZOfDdLE4ODvM7QsN1F1a\n"
"o1KEf9E1KAJLyoUYv0kTZMrPkUfw5h3kZfhIk5izrZkPzd4+o+ncfCGJXoq4/erz\n"
"uRSU/rSPSWQZ4Rn8p9g9XxPbUO8UZ2WS+z+7HQts1mq8taIgm33paYZ0xcFLOrXA\n"
"pXvlt5QPSCoEFAZtkcxckYTinp2qJLxL8nez+2Ad6no+l5S/OxltNZiuYBGbwMap\n"
"Mb7TkgAxX78b5/Qb4hni+Z1bYJ6oIWd6wPZFwbI9nIfh6si8gcwSmgS74XTImGq0\n"
"4QswJ/ZGA60Z7IVPxMvQOpqEKl3c42n1mwIDAQABo1UwUzAMBgNVHRMBAf8EAjAA\n"
"MBMGA1UdJQQMMAoGCCsGAQUFBwMBMA8GA1UdDwEB/wQFAwMHsAAwHQYDVR0OBBYE\n"
"FClRUl1jtO+cT4H0k+lzDavGYJQQMA0GCSqGSIb3DQEBCwUAA4IBgQA6ZfZ42qEC\n"
"8+NYDLaebdGKfB6rHD2CSgvaA5gOPEp/aORPUGGiREN8dXRPjbec/K73y9ufFdaS\n"
"dGMeCziRhyBox1HK6r+1E1I0ax4T2I4cHc3Afvc6rGs5cvw/FIpCt51BAXC8d6Xa\n"
"PxMM2DB4F33T3YFzBaZGFnkvIX7F2aB/sClf1+DcarXLa05M3iDIvIvDhUUloAeR\n"
"XkR89I/eAEUFpe5qgZPo0i0qh2UvcvdJWV8iuQEp3zpOLVmR7babUX99kBtw2Q2e\n"
"NE4yBJSooZ4uh/XA7abkj7C0fATK+oG1xgcxT/lPez6SUFwa+VBQXiCRgDAJsb1N\n"
"e+369zrRODs8MXy0SSc+EM4nlRgehu/OlylZOeLfVQQOMxaZV/xkjnuEaAfWT7iD\n"
"qCee083jVOEShDlSZib+UfEVVx67mc7W88T9mimTCeACck1Tt95nUiYn6QHz57Os\n"
"C9iUNJf/fdZ1WBLV8V6YUmgmU1j3VPe5fnMumXyMwcDnJZtdMvbAb04=\n"
"-----END CERTIFICATE-----\n"
;

static char key[] =
"-----BEGIN RSA PRIVATE KEY-----\n"
"MIIG5AIBAAKCAYEA8Lyp2GXLTAVQv4Qbm3RZyMc+vvLUEqjQo9Bl0GyRTYSSag3c\n"
"v3a/ftUNjpIWtqURSKoU9TUNJQVER7yARmI1RuNAxq2LgJnJRkzWLgdiNN3XlYsX\n"
"gc96mhVxo2JllIrcCCO3SIE6+4XXtlcHgYlcjMaiNHfP6PcUYhEePS9ZbvzaERc9\n"
"wK25+ww5Wlw4mwyaoxthLrjS3Q9k58N0sTg4O8ztCw3UXVqjUoR/0TUoAkvKhRi/\n"
"SRNkys+RR/DmHeRl+EiTmLOtmQ/N3j6j6dx8IYleirj96vO5FJT+tI9JZBnhGfyn\n"
"2D1fE9tQ7xRnZZL7P7sdC2zWary1oiCbfelphnTFwUs6tcCle+W3lA9IKgQUBm2R\n"
"zFyRhOKenaokvEvyd7P7YB3qej6XlL87GW01mK5gEZvAxqkxvtOSADFfvxvn9Bvi\n"
"GeL5nVtgnqghZ3rA9kXBsj2ch+HqyLyBzBKaBLvhdMiYarThCzAn9kYDrRnshU/E\n"
"y9A6moQqXdzjafWbAgMBAAECggGBAM66xQP+zmj2+5OrR+XkMaH4cAqcDsADVkAG\n"
"mxgz00eFPBJ02wdUWzf4j47KJ1UrRT9oR10W9LXA4xTTbDiE54l7Z8n1iCGkbrK/\n"
"EwIt9wi9JP/XlRU1bexZ099hhSfdYvxeZ2uNBnCuTELaU6jKo76EaRCzfshpPYjF\n"
"eHlEawGjg0Q/+Bi5V0eeBLZzEW0cksLUpUzxDKsnKBjawR/azneUEE94zdBpIG2h\n"
"OP4YLsZh/YT0bne7fsenHfRwi7xJwBjBIECCWWrDxK/RjH3OkLn8na6A3BUhwUcK\n"
"kPhmNrxtiiJc/aiOlS5SAGMs1Dq6njwNe9zHdqPoU4bm2VXAEjJEquZZhlCosNNc\n"
"brZMIIgkzYoR07Y9PJGLimtl0MWgIY8ZNKlFpC88VnRWMnxFOUziajIr+efqIW97\n"
"GmdKoFZHcUE3zFNta3LBvWVPzREBsNanG8aDClrLgY/TCz4Lkhz0Fgp8EcC+8chz\n"
"fS79H1AfLOxv+C3lWK1DPJqSp7qx2QKBwQD/LknsKbjWWGULS/ABsIeasq/jp6mZ\n"
"guyfBMck5suIomJMq4pTmSfKKDPG25LFfJ/vOc7cUOSqHRCcbuTVo/IyTo4fK6k2\n"
"XdEr4iwYmkHz4+rZg9foj/zGwRsOH75n3doviG/JtvzCqAqfxWzCgdl2TTFxGKb/\n"
"qKHCRSrpmux6pAMjo0ePGgrObZwMR/a11T+S7+w9S+qRjNWQBTnzOjz9SH6xc3FB\n"
"0qVMslKmzzMQtCgTNzOpqC0zDEuy9+I/xdcCgcEA8YKBKVc0spJwiTrMnmZJP+BU\n"
"6NJ7H6oJ2a2He68HEDeyQhLyfSYHo9d7jDnC357iLq1decxkxCv7Lev5GAcz7sIG\n"
"gtbqv4O4IfmAN1g6D8OtQQV704BLZ2h/YEl3HzEWAakmxDhsqic4kdicJEr9lUmL\n"
"38C48+vM36DSOA6wggcdI4Iy+GJPBw4eNHl+GdnKJgXQhMGoAE3LTXbzoqXAy3hE\n"
"Qs1ctD24ZY2GCpupvsLgN7jd/8PwxX8FZF6Kqc3dAoHAML3AtPFLMvXzZvL8RvZX\n"
"EH9tBUjzJjVRyRX6i9vhqJmLuSVS6KDqbry7FxK9uCcTzXf3QTHaW3vtvlOYNg54\n"
"po9GqkGGsrG80GsFDTX5vwIby6xZQzythbFA37PEhMZldIrU/2yKXwwF63qkSYrP\n"
"5L7d4MJas56cNyVLCw/id5J4XwhDFNxekAtzsQzV3Ol8mS5mq1ai2WZTLI0zAnhv\n"
"SdndCTwJuA7qL/onu2D8WgZvWSxEG/XZnFSO6QJcHt5FAoHAZjeQJ0krmrD0RIDI\n"
"ffpY4lo2VdxQFFTJmoIhp62q1ahdIC4Yx/NCpIvdVLpVyoPaw1rJB3YE6CqdQxBu\n"
"+0aBKnqgetwvuyMq2eZZ6BLFcEqnl6+Uey3/vCK0VrKBYohKAiXvrHkdNN8oyEHf\n"
"xFShA4B/XRKatVKGAdh1YRiGiGIuaQsAO7SQMjI9goQxZQuSzYkEekvkqUxD0eOY\n"
"tqxk7zlV2thEdlzxILpHk1HTBFRCxhLOkyQBUfWy+IozMi9ZAoHBANIJe177D1ob\n"
"hV75fkkcVEcC7RATycQ+6fqAmy3HzGTKcvAQpRVjnWW0Uu2qR1lwInjKv5qoQ++l\n"
"kn1Nld4Yven5KU3i3E8QyaAOBwLtAKufdhlv5qPa+pABEvNzF1lFLF4LNP0mPdsF\n"
"zO5Cwicx+9YUjPheeSAQTdEwryftQbfTPvcnqywlj9AVTXBSmXBGvIlEeNC9HWt2\n"
"sCymw2LvaebbFixyzFVJibtVjYY//x/FcZaxDjEQoJ69gJUVQgri1g==\n"
"-----END RSA PRIVATE KEY-----\n"
;

struct credset *creds;
struct uidset *uids;




#define ARG(con,name)  MHD_lookup_connection_value(con, MHD_GET_ARGUMENT_KIND | MHD_POSTDATA_KIND, name)

int it(void *cls, enum MHD_ValueKind kind, const char *key, const char *value)
{
	printf("   %d-%s: %s\n", (int)kind, key, value);
	return MHD_YES;
}

void dump(struct MHD_Connection *connection, const char *str, int code)
{
	printf("\nFROM-QUERY\n");
	MHD_get_connection_values(connection, MHD_HEADER_KIND|MHD_GET_ARGUMENT_KIND|MHD_POSTDATA_KIND, it, NULL);
	printf("REPLY [%d]\n", (int)code);
	printf("%s\n\n", str);
}

/**
 * Send an HTML reply
 */
void html_reply(
	struct MHD_Connection *connection,
	int code,
	const char *format,
	...
) {
	char *str;
	int rc;
	va_list ap;
	struct MHD_Response *resp;

	va_start(ap, format);
	rc = vasprintf(&str, format, ap);
	va_end(ap);
#if DUMP
	dump(connection, str, code);
#endif
	resp = MHD_create_response_from_buffer(rc, str, MHD_RESPMEM_MUST_FREE);
	MHD_add_response_header(resp, MHD_HTTP_HEADER_CONTENT_TYPE, "text/html");
	MHD_queue_response(connection, code ?: MHD_HTTP_OK, resp);
	MHD_destroy_response(resp);
}

/**
 * Send a JSON reply
 */
void json_reply(
	struct MHD_Connection *connection,
	int code,
	struct json_object *obj
) {
	char *str;
	size_t sz;
	struct MHD_Response *resp;

	str = strdup(json_object_to_json_string(obj));
	sz = strlen(str);
#if DUMP
	dump(connection, str, code);
#endif
	resp = MHD_create_response_from_buffer (sz, str, MHD_RESPMEM_MUST_FREE);
	MHD_add_response_header(resp, MHD_HTTP_HEADER_CONTENT_TYPE, "application/json");
	MHD_queue_response(connection, code ?: MHD_HTTP_OK, resp);
	MHD_destroy_response(resp);
	json_object_put(obj);
}

/****************************************************************************/
/*** HANDLE SMACK LABELS                                                  ***/
/****************************************************************************/

/**
 * returns the smack label of the connection
 */
char *smack_of_connection(struct MHD_Connection *connection)
{
	int fd, rc;
	char *result;
	const union MHD_ConnectionInfo *info;
	socklen_t length;
	char label[1025];

	info = MHD_get_connection_info (connection, MHD_CONNECTION_INFO_CONNECTION_FD);
	result = NULL;
	if (info != NULL) {
		fd = info->connect_fd;

		/* get the security label */
		length = (socklen_t)(sizeof label - 1);
		rc = getsockopt(fd, SOL_SOCKET, SO_PEERSEC, label, &length);
#if UNKNOWN
		if (rc < 0 && errno == ENOPROTOOPT) {
			strcpy(label, "User::App::_UNKNOW_");
			length = (socklen_t)strlen(label);
			rc = 0;
		}
#endif
		if (rc == 0) {
			if (length < (socklen_t)(sizeof label)) {
				label[length] = 0;
				result = strdup(label);
			}
		}
	}
	return result;
}

/**
 * Check that the smack label is correct
 */
int smack_label_is_acceptable(const char *label)
{
	static const char prefix[] = "User::App::";
	return !strncmp(label, prefix, sizeof prefix - 1);
}

/****************************************************************************/
/*** MANAGE CYNAGORA DATABASE                                             ***/
/****************************************************************************/

cynagora_t *cynagora = NULL;

int cynenter()
{
	int rc;

	rc = cynagora ? 0 : cynagora_create(&cynagora, cynagora_Admin, 0, NULL);
	if (rc >= 0)
		rc = cynagora_enter(cynagora);
	return rc;
}

void cynleave(int commit)
{
	if (cynagora)
		cynagora_leave(cynagora, commit);
}

int cynset(const char *client, const char *session, const char *user, const char *permission, const char *value, time_t expire)
{
	int rc;
	cynagora_key_t key;
	cynagora_value_t val;

	key.client = client;
	key.session = session;
	key.user = user;
	key.permission = permission;
	val.value = value;
	val.expire = expire;
	rc = cynagora_set(cynagora, &key, &val);
	return rc;
}

char *escaped(const char *str)
{
	size_t i, j;
	char *result, c;

	j = i = 0;
	while((c = str[i++]))
		j += 1 + (c == '%' || c == ';');

	result = malloc(j + 1);
	if (result) {
		j = i = 0;
		while((c = str[i++])) {
			if (c == '%' || c == ';')
				result[j++] = '%';
			result[j++] = c;
		}
		result[j] = 0;
	}

	return result;
}

int add_token_as_client_rules(const char *token, const char *client, time_t expire)
{
	int rc;
	char *redir, *esclient;

	esclient = escaped(client);
	if (!esclient)
		rc = -ENOMEM;
	else {
		redir = NULL;
		rc = asprintf(&redir, "@:%s;%%s;%%u;%%p", esclient);
		if (rc > 0) {
			rc = cynenter();
			if (rc >= 0) {
				rc = cynset("*", token, "*", "*", redir, expire);
				if (rc >= 0)
					rc = cynset("*", token, "*", "urn:AGL:token:valid", "yes", expire);
			}
			cynleave(rc >= 0);
			free(redir);
		}
		free(esclient);
	}
	return rc;
}

/****************************************************************************/
/*** AUTHORIZATION ENDPOINT                                               ***/
/****************************************************************************/

#if AUTH_CODE
void auth_code_flow(struct MHD_Connection *connection)
{
	const char *state, *clientid, *redirect, *scope;
	char  *bearer;
	struct cred *cred;
	time_t now, t;

	now = time(NULL);
	state = ARG(connection, "state");
	clientid = ARG(connection, "client_id");
	redirect = ARG(connection, "redirect_uri");
	scope = ARG(connection, "scope");

	credset_purge(creds, now);
	credset_new_cred(creds, &cred);
	t = now + 3600;
	cred_set_expire(cred, t);
	cred_get_bearer(cred, now + 180, &bearer);

	html_reply(connection, 200,
			"<html><h1>authorisation code required by &lt;<b>%s</b>&gt;</h1>"
			"<p>scope %s"
			"<p><a href=\"%s?code=%s%s%s\">accept and authorize</a>"
			"<p><a href=\"%s?error=refused%s%s\">refuse</a>"
			, clientid, scope?:""
			, redirect, bearer, state ? "&state=" : "", state ?: ""
			, redirect, state ? "&state=" : "", state ?: "");

	free(bearer);
}
#endif

#if AUTH_IMPL
void auth_implicit_flow(struct MHD_Connection *connection)
{
	const char *state, *clientid, *redirect, *scope;
	char  *bearer;
	struct cred *cred;
	time_t now, t;

	now = time(NULL);
	state = ARG(connection, "state");
	clientid = ARG(connection, "client_id");
	redirect = ARG(connection, "redirect_uri");
	scope = ARG(connection, "scope");

	credset_purge(creds, now);
	credset_new_cred(creds, &cred);
	t = now + 3600;
	cred_set_expire(cred, t);
	cred_get_bearer(cred, now + 180, &bearer);

	html_reply(connection, 200,
			"<html><h1>authorisation implicit required by &lt;<b>%s</b>&gt;</h1>"
			"<p>scope %s"
			"<p><a href=\"%s?token=%s%s%s\">accept and authorize</a>"
			"<p><a href=\"%s?error=refused%s%s\">refuse</a>"
			, clientid, scope?:""
			, redirect, bearer, state ? "&state=" : "", state ?: ""
			, redirect, state ? "&state=" : "", state ?: "");

	free(bearer);
}
#endif

void auth(struct MHD_Connection *connection)
{
#if AUTH_CODE || AUTH_IMPL
	const char *type;

	type = ARG(connection, "response_type") ?: "";
#endif

#if AUTH_CODE
	/* authorization code flow  */
	if (!strcmp(type, "code")) {
		auth_code_flow(connection);
		return;
	}
#endif

#if AUTH_IMPL
	/* implicit flow  */
	if (!strcmp(type, "token")) {
		auth_implicit_flow(connection);
		return;
	}
#endif

	/* unknown flow */
	html_reply(connection, 404, "<html>Bad request");
}

/****************************************************************************/
/*** TOKEN ENDPOINT                                                       ***/
/****************************************************************************/

#if TOK_CODE
void token_authorization_code_flow(struct MHD_Connection *connection)
{
	int rc, rcode;
	const char *code, *clientid, *redirect, *scope;
	char *bearer, *token;
	struct json_object *obj;
	struct cred *cred;
	time_t now, t;

	now = time(NULL);
	code = ARG(connection, "code");
	clientid = ARG(connection, "client_id");
	redirect = ARG(connection, "redirect_uri");
	scope = ARG(connection, "scope");

	credset_purge(creds, now);
	obj = json_object_new_object();

	/* continuation of authorization code flow  */
	rc = credset_search_bearer(creds, code, &cred, &t);
	if (rc || t < now) {
		json_object_object_add(obj, "error", json_object_new_string("unauthorized_client"));
		rcode = MHD_HTTP_UNAUTHORIZED;
	} else {
		cred_get_bearer(cred, now + 3600, &bearer);
		cred_get_token(cred, &token);
		json_object_object_add(obj, "token_type", json_object_new_string("bearer"));
		json_object_object_add(obj, "access_token", json_object_new_string(bearer));
		json_object_object_add(obj, "refresh_token", json_object_new_string(token));
		json_object_object_add(obj, "expires_in", json_object_new_int(3600));
		free(bearer);
		free(token);
		rcode = MHD_HTTP_OK;
	}
	return json_reply(connection, rcode, obj);
}
#endif

#if TOK_CLIENT
void token_client_credentials_flow(struct MHD_Connection *connection)
{
	int rc, code;
	char *bearer;
	struct json_object *obj;
	struct cred *cred;
	time_t now, dur, exp;
	char *smack_label = NULL;
	struct uid *uid;

	/* create the object */
	obj = json_object_new_object();

	/* get times */
	now = time(NULL);

	/* purge internals */
	credset_purge(creds, now);

	/* check if smack is allowed */
	smack_label = smack_of_connection(connection);
	if (!smack_label || !smack_label_is_acceptable(smack_label))
		goto forbidden;

	/* retrieves the cred associated to the client */
	uid = uidset_search(uids, smack_label);
	if (uid != NULL) {
		cred = uid_data(uid);
		bearer = cred_data(cred);
		exp = cred_get_expire(cred);
		dur = 5 * 24 * 60 * 60; /* 5 days */
		if (now + dur >= exp) {
			/* remove the current token for creating a new */
			free(bearer);
			cred_unref(cred);
			uid_unref(uid);
			uid = NULL;
		}
	}
	if (uid == NULL) {
		/* create a cred for the client */
		dur = 2 * 365 * 24 * 60 * 60; /* 2 years */
		exp = now + dur;
		rc = credset_new_cred(creds, &cred);
		if (rc < 0)
			goto internal;
		cred_set_expire(cred, exp);

		/* create the client token */
		rc = cred_get_bearer(cred, exp, &bearer);
		if (rc < 0) {
			cred_unref(cred);
			goto internal;
		}
		cred_set_data(cred, bearer);

		/* record the client */
		rc = uid_create_for_text(&uid, uids, smack_label, cred);
		if (rc < 0) {
			free(bearer);
			cred_unref(cred);
			goto internal;
		}

		/* add rules to cynagora */
		rc = add_token_as_client_rules(bearer, smack_label, exp);
		if (rc < 0) {
			free(bearer);
			cred_unref(cred);
			uid_unref(uid);
			goto internal;
		}
	}

	json_object_object_add(obj, "token_type", json_object_new_string("bearer"));
	json_object_object_add(obj, "access_token", json_object_new_string(bearer));
	json_object_object_add(obj, "expires_in", json_object_new_int((int32_t)(exp - now)));
	code = MHD_HTTP_OK;
	goto end;

internal:
	json_object_object_add(obj, "error", json_object_new_string("internal_error"));
	code = MHD_HTTP_INTERNAL_SERVER_ERROR;
	goto end;

forbidden:
	json_object_object_add(obj, "error", json_object_new_string("unauthorized_client"));
	code = MHD_HTTP_UNAUTHORIZED;
	goto end;

end:
	free(smack_label);
	return json_reply(connection, code, obj);
}
#endif

void token(struct MHD_Connection *connection)
{
	struct json_object *obj;

#if TOK_CODE || TOK_CLIENT
	const char *type;

	type = ARG(connection, "grant_type") ?: "";
#endif

#if TOK_CODE
	/* continuation of authorization code flow  */
	if (!strcmp(type, "authorization_code")) {
		token_authorization_code_flow(connection);
		return;
	}
#endif

#if TOK_CLIENT
	/* special client credentials */
	if (!strcmp(type, "client_credentials")) {
		token_client_credentials_flow(connection);
		return;
	}
#endif

	/* unknown flow */
	obj = json_object_new_object();
	json_object_object_add(obj, "error", json_object_new_string("invalid_request"));
	json_reply(connection, 200, obj);
}

/****************************************************************************/
/*** SERVE AND ROUTE                                                      ***/
/****************************************************************************/

void request_completed (
		void *cls,
		struct MHD_Connection *connection,
		void **con_cls,
		enum MHD_RequestTerminationCode toe
) {
	free(*con_cls);
}

int access_handler(
		void *cls,
		struct MHD_Connection *connection,
		const char *url,
		const char *method,
		const char *version,
		const char *upload_data,
		size_t *upload_data_size,
		void **con_cls
) {
	if (!strcasecmp(method, MHD_HTTP_METHOD_POST) && !*con_cls) {
		if (*upload_data_size) {
			const char **args; int i;
			*con_cls = args = unescape_args_size(upload_data, *upload_data_size);
			*upload_data_size = 0;
			for (i = 0 ; args[i] ; i+=2) {
				MHD_set_connection_value (connection, MHD_GET_ARGUMENT_KIND, args[i], args[i + 1]);
			}
		}
		return MHD_YES;
	}

	if (!strcasecmp(url, "/auth"))
		auth(connection);
	else if (!strcasecmp(url, "/tok"))
		token(connection);
	else
		html_reply(connection, 404, "<html>%s not found", url);
	return MHD_YES;
}

/****************************************************************************/
/*** SETTING                                                              ***/
/****************************************************************************/

int openhost(const char *hostname)
{
	int rc, fd;
	char *host, *service;
	struct addrinfo hint, *rai, *iai;

	/* scan the uri */
	host = strdup(hostname);
	service = strchr(host, ':');
	if (service == NULL)
		service = DEFAULTPORT;
	else
		*service++ = 0;

	/* get addr */
	memset(&hint, 0, sizeof hint);
	hint.ai_family = AF_INET;
	hint.ai_socktype = SOCK_STREAM;
	hint.ai_flags = AI_PASSIVE;
	if (host[0] == 0 || (host[0] == '*' && host[1] == 0))
		host = NULL;
	rc = getaddrinfo(host, service, &hint, &rai);
	if (rc != 0) {
		fprintf(stderr, "Can't get info about %s", hostname);
		exit(10);
	}

	/* get the socket */
	iai = rai;
	while (iai != NULL) {
		fd = socket(iai->ai_family, iai->ai_socktype, iai->ai_protocol);
		if (fd >= 0) {
			rc = 1;
			setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &rc, sizeof rc);
			rc = bind(fd, iai->ai_addr, iai->ai_addrlen);
			if (rc == 0) {
				rc = listen(fd, 3);
				if (rc== 0) {
					freeaddrinfo(rai);
					return fd;
				}
			}
			close(fd);
		}
		iai = iai->ai_next;
	}
	freeaddrinfo(rai);
	fprintf(stderr, "Not able to open server %s", hostname);
	exit(11);
	return -1;
}

int openhosts(const char *hostnames, struct pollfd *fds, int count)
{
	int r;
	char *hosts;
	size_t s;
	static const char seps[] = " \n\t;";

	hostnames += strspn(hostnames, seps);
	hosts = strdup(hostnames);
	r = 0;
	for(;;) {
		s = strcspn(hosts, seps);
		if (!s)
			return r;
		if (r == count) {
			fprintf(stderr, "too many listening adresses");
			exit(9);
		}
		if (hosts[s])
			hosts[s++] = 0;
		fds[r].fd = openhost(hosts);
		fds[r++].events = POLLIN;
		hosts += s;
		hosts += strspn(hosts, seps);
	}
}

void makeconnection(struct MHD_Daemon *mhd, int fd)
{
	int con, sts;
	struct sockaddr addr;
	socklen_t lenaddr;

	lenaddr = (socklen_t)sizeof addr;
	con = accept(fd, &addr, &lenaddr);
	if (con < 0) {
		fprintf(stderr, "can't accept incoming connection");
		exit(6);
	}
	sts = MHD_add_connection(mhd, con, &addr, lenaddr);
	if (sts != MHD_YES) {
		fprintf(stderr, "can't add incoming connection");
		exit(7);
	}
}

void main(int ac, char **av)
{
	struct MHD_Daemon *mhd;
	const union MHD_DaemonInfo *info;
	MHD_UNSIGNED_LONG_LONG to;
	int i, r, n, s, o;
	unsigned int flags;
	struct pollfd pfds[20];

	credset_create(&creds);
	uidset_create(&uids, 50);

	flags = 0;
	while((o = getopt_long(ac, av, shortopts, longopts, NULL)) >= 0) {
		switch (o) {
		case 'h':
			printhelp(av[0]);
			break;
		case 's':
			flags |= MHD_USE_TLS;
			break;
		case 'u':
			flags &= ~MHD_USE_TLS;
			break;
		default:
			fprintf(stderr, "Bad option detected");
			exit(1);
			break;
		}
	}

	/* open hosts */
	s = (int)(sizeof pfds / sizeof *pfds);
	n = 1;
	if (!av[optind])
		n += openhosts(getenv(ENVHOSTS)?:DEFAULTHOSTS, &pfds[n], s - n);
	else 
		while (av[optind])
			n += openhosts(av[optind++], &pfds[n], s - n);
	if (n == 0) {
		fprintf(stderr, "not listening");
		exit(1);
	}

	/* instanciate LMHD */
	mhd = MHD_start_daemon(
			  MHD_USE_EPOLL
			| MHD_USE_DEBUG
			| MHD_USE_NO_LISTEN_SOCKET
			| flags,
			-1,
			NULL, NULL,
			access_handler, NULL,
			MHD_OPTION_NOTIFY_COMPLETED, &request_completed, NULL,
			flags ? MHD_OPTION_HTTPS_MEM_KEY : MHD_OPTION_END, key,
			MHD_OPTION_HTTPS_MEM_CERT, cert,
			MHD_OPTION_END);
	if (NULL == mhd) {
		fprintf(stderr, "cant't create http server\n");
		exit(1);
	}

	/* add LMHD to polls */
        info = MHD_get_daemon_info(mhd, MHD_DAEMON_INFO_EPOLL_FD);
        if (info == NULL) {
		fprintf(stderr, "cant't get http server\n");
                MHD_stop_daemon(mhd);
		exit(2);
        }
        pfds[0].fd = info->epoll_fd;
        pfds[0].events = POLLIN;

	/* main loop */
        for(;;) {
		/* get timeout */
                if (MHD_get_timeout(mhd, &to) == MHD_NO)
                        i = -1;
                else
                        i = (int)to;

		/* wait */
                r = poll(pfds, n,  i);
		if (r < 0) {
			fprintf(stderr, "poll error\n");
			MHD_stop_daemon (mhd);
			exit(3);
		}

		/* dispatch LMHD */
		if (r == 0 || (pfds[0].revents & POLLIN))
                	MHD_run(mhd);
		if (pfds[0].revents & POLLHUP) {
			fprintf(stderr, "epoll hung\n");
			MHD_stop_daemon (mhd);
			exit(4);
		}

		/* dispatch connections listeners */
		for (i = 1 ; i < n ; i++) {
			if (pfds[i].revents & POLLIN)
				makeconnection(mhd, pfds[i].fd);
			if (pfds[i].revents & POLLHUP) {
				fprintf(stderr, "listen hung\n");
			        MHD_stop_daemon (mhd);
				exit(5);
			}
		}
        }
}
