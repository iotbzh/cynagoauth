/*
 * Copyright (C) 2019 "IoT.bzh"
 * Author: José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdint.h>
#include <stdlib.h>
#include <limits.h>

#include <stdio.h>
#include <string.h>

#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/random.h>

#include "uid.h"
#include "cred.h"
#include "base64.h"
#include "crypt.h"


/*
 * token-court: crypt(salt, id-token-long, expiration)
 */

#define MAGIC_V1   0x70832

#define TOKLEN     64
#define IDLEN      4
#define CDCHAR     'A'
#define MAXSTRLEN  2047
#define MAXTIMEBIT 48

struct scope;

struct scope {
	unsigned refcount;
	char name[1];
};

struct scopearray {
	unsigned count;
	unsigned capacity;
	struct scope **scopes;
};

struct cred {
	struct credset *set;
	struct uid *token;
	void *data;
	uint32_t id;
	unsigned refcount;
	time_t expiration;
	int locked;
	struct scopearray scopes;
};

union bearer {
	uint64_t      u64[3];
	uint32_t      u32[6];
	uint16_t      u16[12];
	uint8_t       u8[24];
	unsigned char uc[24];
	char          c[24];
};

struct credset {
	struct uidset *tokens;
	struct scopearray scopes;
	unsigned refcount;
	struct crypt *crypt;
};

/******************************************************************************/

static inline void getrand(void *buffer, size_t length)
{
	ssize_t rgr __attribute__ ((unused));
	rgr = getrandom(buffer, length, 0);
}

void bearer_encode(union bearer *bearer, struct credset *credset, uint32_t id, time_t time)
{
	/* 0 / 0..1 / 0..3 / 0..7 */
#if MAXTIMEBIT <= 48
	bearer->u32[0] = (uint32_t)time;
	bearer->u16[2] = (uint16_t)(time >> 32);
	getrand(&bearer->u16[3], sizeof bearer->u16[3]);

#else
	bearer->u64[0] = (uint64_t)time;
#endif
	/* 1 / 2..3 / 4..7 / 8..15 */
	bearer->u32[2] = id;
	bearer->u16[6] = 0;
	getrand(&bearer->u16[7], sizeof bearer->u16[7]);

	/* 2 / 4..5 / 8..11 / 16..23 */
	getrand(&bearer->u64[2], sizeof bearer->u64[2]);

	crypt_encrypt(credset->crypt, bearer, sizeof *bearer, 1);
}

int bearer_decode(union bearer *bearer, struct credset *credset, uint32_t *id, time_t *time)
{
	crypt_decrypt(credset->crypt, bearer, sizeof *bearer, 1);

	if (bearer->u16[6])
		return -EINVAL;

	*id = bearer->u32[2];
#if MAXTIMEBIT <= 48
	*time = (time_t)((uint64_t)bearer->u32[0] |
			(((uint64_t)bearer->u16[2]) << 32));
#else
	*time = (time_t)bearer->u64[0];
#endif
	return 0;
}


/**
 * Search the scope of 'name' in the array 'scopes'
 * Returns in *index (if not NULL) the index of the scope or its insertion
 * place
 * @param scopes array of scopes
 * @param name name of the scope to search
 * @param index where to store the index or insertion index
 * @return the found scope or NULL if not found
 */
static struct scope *scopes_search(
				const struct scopearray *scopes,
				const char *name,
				unsigned *index
) {
	int c;
	unsigned low, up, mid;
	struct scope *s;

	low = 0;
	up = scopes->count;
	while (low < up) {
		mid = (low + up) >> 1;
		s = scopes->scopes[mid];
		c = strcasecmp(s->name, name);
		if (c < 0)
			low = mid + 1;
		else if (c > 0)
			up = mid;
		else {
			if (index)
				*index = mid;
			return s;
		}
	}
	if (index)
		*index = low;
	return NULL;
}

/**
 * Drop the element of 'scopes' of 'index'
 * @param scopes array of scopes
 * @param index to drop
 */
static void scopes_drop(struct scopearray *scopes, unsigned index)
{
	scopes->count--;
	while (index < scopes->count) {
		scopes->scopes[index] = scopes->scopes[index + 1];
		index++;
	}
}

/**
 * Insert the element 'scope' in 'scopes' at 'index'
 * @param scopes array of scopes
 * @aram scope the scope to insert
 * @param index of insertion
 * @return 0 in case of success or -ENOMEM on error
 */
static int scopes_insert(struct scopearray *scopes, struct scope *scope, unsigned index)
{
	unsigned capacity;
	struct scope *other, **array;

	capacity = scopes->capacity;
	if (scopes->count == capacity) {
		capacity = (capacity << 1) ?: 4;
		array = realloc(scopes->scopes, capacity * sizeof *array);
		if (!array)
			return -ENOMEM;
		scopes->scopes = array;
		scopes->capacity = capacity;
	}
	while (index < scopes->count) {
		other = scopes->scopes[index];
		scopes->scopes[index] = scope;
		scope = other;
		index++;
	}
	scopes->scopes[scopes->count++] = scope;
	return 0;
}

/**
 * Allocates a fresh credset
 * @return the allocated credset or NULL if error
 */
static struct credset *allocate()
{
	struct credset *credset = calloc(1, sizeof *credset);
	if (credset) {
		credset->refcount = 1;
		if (uidset_create(&credset->tokens, TOKLEN) < 0) {
			free(credset);
			credset = NULL;
		}
	}
	return credset;
}

/**
 * Deallocate a credset
 * @param result
 * @return
 */
static void deallocate(struct credset *credset)
{
	if (credset) {
		crypt_destroy(credset->crypt);
		uidset_unref(credset->tokens);
		free(credset);
	}
}

static struct cred *search_token(struct credset *credset, const char *token)
{
	struct uid *uid = uidset_search(credset->tokens, token);
	return uid ? uid_data(uid) : NULL;
}

struct searchid {
	uint32_t id;
	struct cred *cred;
};

static void stokid(void *closure, struct uid *uid, void *data)
{
	struct searchid *sid = closure;
	struct cred *cred = data;
	if (cred->id == sid->id)
		sid->cred = cred;
}

static struct cred *search_id(struct credset *credset, uint32_t id)
{
	struct searchid sid = { .id = id, .cred = NULL };
	uidset_for_all(credset->tokens, stokid, &sid);
	return sid.cred;
}

static int make_cred(
		struct credset *credset,
		struct cred **result,
		const char *token,
		uint32_t id
) {
	int rc;
	struct cred *cred;

	if (!id) {
		getrand(&id, sizeof id);
		while (search_id(credset, id) || !id)
			id++;
	}

	cred = malloc(sizeof *cred);
	if (!cred)
		rc = -ENOMEM;
	else {
		cred->set = credset;
		cred->id = id;
		cred->refcount = 1;
		cred->expiration = 0;
		cred->locked = 0;
		cred->scopes.count = 0;
		cred->scopes.capacity = 0;
		cred->scopes.scopes = NULL;
		cred->data = NULL;
		rc = uid_create_for_text(&cred->token, credset->tokens, token, cred);
		if (!rc)
			credset_addref(credset);
		else {
			rc = -errno;
			free(cred);
			cred = NULL;
		}
	}
	*result = cred;
	return rc;
}

/******************************************************************************/

static int store_unsigned(FILE *file, unsigned value, int nl)
{
	return fprintf(file, "%x%c", value, nl ? '\n' : ' ') > 0 ? 0 : -errno;
}

static int store_raw(FILE *file, const void *buffer, size_t length)
{
	return fwrite(buffer, length, 1, file) == 1 ? 0 : -errno;
}

static int store_bin(FILE *file, const void *buffer, size_t length)
{
	const unsigned char *data = buffer;
	char x[2];
	size_t i;
	int rc = 0;

	for (i = 0 ; rc == 0 && i < length ; i++) {
		x[0] = (char)(CDCHAR + ((data[i] >> 4) & 15));
		x[1] = (char)(CDCHAR + (data[i] & 15));
		rc = store_raw(file, x, 2);
	}
	return rc;
}

static int store_item(FILE *file, const void *buffer, size_t length, int string)
{
	int rc;

	if (length > UINT_MAX) {
		rc = -EINVAL;
		goto end;
	}

	rc = store_unsigned(file, (unsigned)length, 0);
	if (rc)
		goto end;

	rc = (string ? store_raw : store_bin)(file, buffer, length);
	if (rc)
		goto end;

	rc = store_raw(file, "\n", 1);
end:
	return rc;
}

static int store_string(FILE *file, const char *string)
{
	return store_item(file, string, strlen(string), 1);
}

static int load_unsigned(FILE *file, unsigned *value)
{
	return fscanf(file, "%x ", value) > 0 ? 0 : -errno;
}

static int load_raw(FILE *file, void *buffer, size_t length)
{
	return fread(buffer, length, 1, file) == 1 ? 0 : -errno;
}

static int load_bin(FILE *file, void *buffer, size_t length)
{
	char *data = buffer;
	char x[2];
	size_t i;
	int rc = 0;

	for (i = 0 ; rc == 0 && i < length ; i++) {
		rc = load_raw(file, x, 2);
		if (!rc)
			data[i] = (char)(((x[0] - CDCHAR) << 4) | (x[1] - CDCHAR));
	}
	return rc;
}

static int load_item(FILE *file, void *buffer, size_t length, int string, unsigned *rlen)
{
	unsigned len;
	int rc;

	rc = load_unsigned(file, &len);
	if (rc || len > length) {
		rc = -EINVAL;
		goto end;
	}

	if (rlen)
		*rlen = len;

	rc = (string ? load_raw : load_bin)(file, buffer, len);
	if (rc)
		goto end;

	fscanf(file, " ");
end:
	return rc;
}

static int load_string(FILE *file, char *string, size_t length, unsigned *rlen)
{
	unsigned len;
	int rc = load_item(file, string, length - 1, 1, &len);
	if (!rc) {
		if (rlen)
			*rlen = len;
		string[len] = 0;
	}
	return rc;
}

struct credfile {
	int rc;
	FILE *file;
};

static void store_cred(void *closure, struct uid *uid, void *data)
{
	unsigned i;
	struct credfile *cf = closure;
	struct cred *cred = data;
	FILE *file = cf->file;
	int rc = cf->rc;

	if (rc)
		return;

	rc = store_string(file, uid_text(uid));
	if (rc)
		goto end;

	rc = store_unsigned(file, (unsigned)cred->id, 1);
	if (rc)
		goto end;

	rc = store_item(file, &cred->expiration, sizeof cred->expiration, 0);
	if (rc)
		goto end;

	rc = store_unsigned(file, cred->scopes.count, 1);
	for (i = 0 ; !rc && i < cred->scopes.count ; i++)
		rc = store_string(file, cred->scopes.scopes[i]->name);

end:
	cf->rc = rc;
}

static int load_cred(FILE *file, struct credset *credset)
{
	char buffer[MAXSTRLEN + 1];
	unsigned i, n, len;
	int rc;
	struct cred *cred;
	time_t expire;
	uint32_t id;

	rc = load_string(file, buffer, sizeof buffer, &len);
	if (rc)
		goto end;

	rc = load_unsigned(file, &i);
	if (rc)
		goto end;

	id = (uint32_t)i;
	rc = load_item(file, &expire, sizeof expire, 0, NULL);
	if (rc)
		goto end;

	cred = search_token(credset, buffer) ?: search_id(credset, id);
	if (cred) {
		rc = -EINVAL;
		goto end;
	}

	rc = make_cred(credset, &cred, buffer, id);
	if (rc)
		goto end;

	rc = load_unsigned(file, &n);
	for (i = 0 ; !rc && i < n ; i++) {
		rc = load_string(file, buffer, sizeof buffer, &len);
		if (!rc)
			rc = cred_add_scope(cred, buffer);
	}

end:
	return rc;
}

static int storefile(FILE *file, struct credset *credset)
{
	int rc;
	struct credfile cf;

	rc = store_unsigned(file, MAGIC_V1, 1);
	if (!rc)
		rc = store_unsigned(file, uidset_count(credset->tokens), 1);
	if (!rc) {
		cf.rc = 0;
		cf.file = file;
		uidset_for_all(credset->tokens, store_cred, &cf);
		rc = cf.rc;
	}
	return rc;
}

static int loadfile(FILE *file, struct credset *credset)
{
	unsigned i, n;
	int rc;

	rc = load_unsigned(file, &n);
	if (!rc && n != MAGIC_V1)
		rc = -EINVAL;
	if (!rc)
		rc = load_unsigned(file, &n);
	for (i = 0 ; !rc && i < n ; i++)
		rc = load_cred(file, credset);
	return rc;
}

static int store(struct credset *credset, const char *filename)
{
	FILE *file;
	int rc;
	mode_t m;

	m = umask(0077);
	file = fopen(filename, "w");
	umask(m);
	if (!file)
		rc = -errno;
	else {
		rc = storefile(file, credset);
		fclose(file);
	}
	return rc;
}

static int load(struct credset *credset, const char *filename)
{
	FILE *file;
	int rc;

	file = fopen(filename, "r");
	if (!file)
		rc = -errno;
	else {
		rc = loadfile(file, credset);
		fclose(file);
	}
	return rc;
}

/******************************************************************************/

/**
 * Create a new fresh credset
 * @param result where to put the created credset
 * @return 0 in case of success or a negative value
 */
int credset_create(struct credset **result)
{
	int rc;
	struct credset *credset;

	*result = credset = allocate();
	if (!credset)
		rc = -ENOMEM;
	else {
		rc = crypt_create(&credset->crypt, sizeof(union bearer));
		if (rc) {
			*result = NULL;
			deallocate(credset);
		}
	}
	return rc;
}

/**
 * Create the credset from the content of 'filename'
 * @param result the loaded credset
 * @param filename the file to load
 * @return 0 in case of success or a negative value
 */
int credset_load(struct credset **result, const char *filename)
{
	int rc;
	struct credset *credset;

	*result = credset = allocate();
	if (!credset)
		rc = -ENOMEM;
	else {
		rc = load(credset, filename);
		if (rc < 0) {
			*result = NULL;
			deallocate(credset);
		}
	}
	return rc;
}

/**
 * Store the credset to the 'filename'
 * @param credset the credset to store
 * @param filename the file to create
 * @return 0 in case of success or a negative value
 */
int credset_store(struct credset *credset, const char *filename)
{
	return store(credset, filename);
}

struct credset *credset_addref(struct credset *credset)
{
	if (credset)
		__atomic_add_fetch(&credset->refcount, 1, __ATOMIC_RELAXED);
	return credset;
}

void credset_unref(struct credset *credset)
{
	if (credset && !__atomic_sub_fetch(&credset->refcount, 1, __ATOMIC_RELAXED))
		deallocate(credset);
}

struct itercred {
	void (*callback)(void *closure, struct cred *cred);
	void *closure;
};

static void for_all_cred(void *closure, struct uid *uid, void *data)
{
	struct itercred *it = closure;
	struct cred *cred = data;
	it->callback(it->closure, cred);
}

void credset_for_all(
	struct credset *credset,
	void (*callback)(void *closure, struct cred *cred),
	void *closure
) {
	struct itercred it = { .callback = callback, .closure = closure };
	uidset_for_all(credset->tokens, for_all_cred, &it);
}

static void purge_cred(void *closure, struct uid *uid, void *data)
{
	time_t *time = closure;
	struct cred *cred = data;
	if (cred_is_expired(cred, *time))
		cred_revoke(cred);
}

void credset_purge(struct credset *credset, time_t time)
{
	uidset_for_all(credset->tokens, purge_cred, &time);
}

int credset_new_cred(struct credset *credset, struct cred **result)
{
	return make_cred(credset, result, NULL, 0);
}

int credset_search_token(struct credset *credset, const char *token, struct cred **result, time_t *expire)
{
	int rc;
	struct uid *uid;
	struct cred *cred;

	uid = uidset_search(credset->tokens, token);
	if (!uid) {
		cred = NULL;
		rc = -ENOENT;
	} else {
		cred = uid_data(uid);
		rc = 0;
	}
	if (result)
		*result = cred;
	if (expire)
		*expire = cred ? cred->expiration : 0;
	return rc;
}

int credset_search_bearer(struct credset *credset, const char *bearer, struct cred **result, time_t *expire)
{
	char *pout;
	int sta, rc;
	union bearer b;
	size_t blen;
	struct cred *cred;
	uint32_t id;
	time_t time;

	if (result)
		*result = NULL;
	if (expire)
		*expire = 0;

	blen = strlen(bearer);
	if (base64_encode_length(sizeof b.c, Base64_Variant_URL_Trunc) != blen)
		return -EINVAL;

	pout = b.c;
	sta = 0;
	base64_decode_put(bearer, blen, &pout, &sta);
	if (pout - b.c != sizeof b.c)
		return -EINVAL;

	rc = bearer_decode(&b, credset, &id, &time);
	if (rc)
		return rc;

	cred = search_id(credset, id);
	if (result)
		*result = cred;
	if (expire)
		*expire = time;
	return 0;

}

int credset_search(struct credset *credset, const char *code, struct cred **result, time_t *expire)
{
	int rc;

	rc = credset_search_bearer(credset, code, result, expire);
	if (rc)
		rc = credset_search_token(credset, code, result, expire);
	return rc;
}

struct cred *cred_addref(struct cred *cred)
{
	if (cred)
		__atomic_add_fetch(&cred->refcount, 1, __ATOMIC_RELAXED);
	return cred;
}

void cred_unref(struct cred *cred)
{
	if (cred)
		__atomic_sub_fetch(&cred->refcount, 1, __ATOMIC_RELAXED);
}

int cred_add_scope(struct cred *cred, const char *scopename)
{
	int rc;
	unsigned index, sindex;
	struct scope *s;
	size_t slen;

	if (cred->locked)
		return -EPERM;

	s = scopes_search(&cred->scopes, scopename, &index);
	if (s)
		return 0;

	s = scopes_search(&cred->set->scopes, scopename, &sindex);
	if (!s) {
		slen = strlen(scopename);
		if (slen > MAXSTRLEN)
			return -EINVAL;
		s = malloc(slen + sizeof *s);
		if (!s)
			return -ENOMEM;
		s->refcount = 0;
		memcpy(s->name, scopename, slen + 1);
		rc = scopes_insert(&cred->set->scopes, s, sindex);
		if (rc) {
			free(s);
			return rc;
		}
	}

	__atomic_add_fetch(&s->refcount, 1, __ATOMIC_RELAXED);
	rc = scopes_insert(&cred->scopes, s, index);
	if (rc && !__atomic_sub_fetch(&s->refcount, 1, __ATOMIC_RELAXED)) {
		scopes_drop(&cred->set->scopes, sindex);
		free(s);
	}
	return rc;
}

int cred_sub_scope(struct cred *cred, const char *scopename)
{
	unsigned index;
	struct scopearray *scopes;
	struct scope *s;

	if (cred->locked)
		return -EPERM;

	scopes = &cred->scopes;
	s = scopes_search(scopes, scopename, &index);
	if (!s)
		return -ENOENT;

	scopes_drop(scopes, index);
	if (!__atomic_sub_fetch(&s->refcount, 1, __ATOMIC_RELAXED)) {
		scopes = &cred->set->scopes;
		if (s == scopes_search(scopes, scopename, &index))
			scopes_drop(scopes, index);
		free(s);
	}
	return 0;
}

int cred_has_scope(struct cred *cred, const char *scopename)
{
	return !!scopes_search(&cred->scopes, scopename, NULL);
}

void cred_revoke(struct cred *cred)
{
	struct uid *token = cred->token;

	cred->token = NULL;
	if (token) {
		uid_unref(token);
		cred_unref(cred);
	}
}

void cred_set_expire(struct cred *cred, time_t expire)
{
	cred->expiration = expire;
}

time_t cred_get_expire(struct cred *cred)
{
	return cred->expiration;
}

int cred_is_expired(struct cred *cred, time_t time)
{
	return cred->expiration && cred->expiration < time;
}

int cred_get_token(struct cred *cred, char **result)
{
	char *token = strdup(uid_text(cred->token));
	*result = token;
	return token ? 0 : -ENOMEM;
}

int cred_get_bearer(struct cred *cred, time_t expire, char **result)
{
	char *bearer;
	int sta;
	union bearer b;

	if (expire >> MAXTIMEBIT)
		return -EINVAL;

	bearer = malloc(1 + base64_encode_length(sizeof b.c, Base64_Variant_URL_Trunc));
	*result = bearer;
	if (!bearer)
		return -ENOMEM;

	bearer_encode(&b, cred->set, cred->id, expire);

	sta = 0;
	base64_encode_put(b.c, sizeof b.c, &bearer, &sta, Base64_Variant_URL_Trunc);
	base64_encode_flush(&bearer, &sta, Base64_Variant_URL_Trunc);
	*bearer = 0;

	cred->locked = 1;
	return 0;
}

void *cred_data(struct cred *cred)
{
	return cred->data;
}

void cred_set_data(struct cred *cred, void *data)
{
	cred->data = data;
}
